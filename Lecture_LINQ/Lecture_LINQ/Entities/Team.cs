﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture_LINQ.Entities
{
    public class Team
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime createdAt { get; set; }
        public override string ToString()
        {
            return $"{name}|Создана: {createdAt}";
        }
    }
}
