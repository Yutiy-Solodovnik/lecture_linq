﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture_LINQ.Entities
{
    public class MyTask
    {
        public int id { get; set; }
        public int projectId { get; set; }
        public int performerId { get; set; }
        public User performer { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int state { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime? finishedAt { get; set; }
        public override string ToString()
        {
            return $"Название: {name}|Разработчик: {performer.firstName + " " + performer.lastName}" +
                $"|Описание {description}|Состояние {state}|Создан: {createdAt}|Завершен: {finishedAt}";
        }
    }
}
