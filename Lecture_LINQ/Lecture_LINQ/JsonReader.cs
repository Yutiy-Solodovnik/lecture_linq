﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Lecture_LINQ.Entities;

namespace Lecture_LINQ
{
    public static class JsonReader
    {
        static JsonReader()
        {

        }
        private static string _baseUri = "https://bsa21.azurewebsites.net/";
        public static List<T> GetEntitiesList<T> () where T : class
        {
            using (HttpClient client = new HttpClient())
            {
                string path = typeof(T).Name switch
                {
                    "Project" => "api/Projects",
                    "User" => "api/Users",
                    "Team" => "api/Teams",
                    "MyTask" => "api/Tasks",
                    _ => throw new ArgumentException("Wrong type")
                }; 
                client.BaseAddress = new Uri(_baseUri);
                Task<HttpResponseMessage> response = client.GetAsync(path);
                response.Wait();
                Task<string> message = response.Result.Content.ReadAsStringAsync();
                message.Wait();
                List<T> entetiesList = JsonConvert.DeserializeObject<List<T>>(message.Result);
                return entetiesList;
            }
        }
    }
}
